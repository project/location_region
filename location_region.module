<?php

/**
 * Supported countries by Location Region.
 */
function location_region_supported_countries() {
  return array(
    'fr' => 'France',
    'lb' => 'Lebanon',
  );
}

/**
 * Check if a country is supported by Location Region.
 */
function location_region_is_country_supported($country) {
  $country = trim(strtolower($country));
  return array_key_exists($country, location_region_supported_countries());
}

/**
 * Implements hook_locationapi().
 */
function location_region_locationapi(&$location, $op, $a3 = NULL, $a4 = NULL, $a5 = NULL) {
  switch ($op) {
    case 'fields':
      return array('region' => t('Region'));

    case 'widget':
      switch ($a3) {
        case 'region':
          return array(
            'select' => 'Dropdown',
          );
        default:
          return array();
      }

    case 'field_expand':
      if (is_array($a4)) {
        $settings = $a4;
      }
      else {
        // On this $op, $a4 is now expected to be an array,
        // but we make an exception for backwards compatibility.
        $settings = array(
          'default' => NULL,
          'weight' => NULL,
          'collect' => $a4,
          'widget' => NULL,
        );
      }

      if ($a3 == 'region') {
        if (!empty($settings['#parents'])) {
          $wrapper_suffix = '-' . implode('-', $settings['#parents']);
        }
        else {
          $settings['#parents'] = array();
          $wrapper_suffix = '';
        }

        return array(
          '#type' => 'select',
          '#title' => t('Region'),
          '#maxlength' => 31,
          '#description' => NULL,
          '#required' => ($a4 == 2),
          '#default_value' => $location,
          '#options' => array('' => t('Select')),
          '#prefix' => '<div id="location-dropdown-region-wrapper' . $wrapper_suffix . '">',
          '#suffix' => '</div>',
          '#ajax' => array(
            'callback' => 'location_region_region_callback',
            'path' => 'system/ajax/' . implode('/', $settings['#parents']),
            'wrapper' => 'location-dropdown-province-wrapper' . $wrapper_suffix,
            'effect' => 'fade',
          ),
        );
      }

      if ($a3 == 'country') {
        // Force default.
        if ($settings['collect'] == 4) {
          return array(
            '#type' => 'value',
            '#value' => $location,
          );
        }
        else {
          $options = array_merge(
            array(
              '' => t('Select'),
              'xx' => t('NOT LISTED'),
            ),
            location_get_iso3166_list()
          );
          if (!empty($settings['#parents'])) {
            $wrapper_suffix = '-' . implode('-', $settings['#parents']);
          }
          else {
            $settings['#parents'] = array();
            $wrapper_suffix = '';
          }

          return array(
            '#type' => 'select',
            '#title' => t('Country'),
            '#default_value' => $location,
            '#options' => $options,
            '#description' => NULL,
            '#required' => ($settings['collect'] == 2),
            // Used by province autocompletion js.
            '#attributes' => array('class' => array('location_auto_country')),
            '#ajax' => array(
              'callback' => 'location_region_country_ajax_callback',
              'wrapper' => 'location-dropdown-region-wrapper' . $wrapper_suffix,
              'effect' => 'fade',
            ),
          );
        }
      }

      break;

    case 'save':
      db_update('location')
        ->fields(array(
          'region' => $location['region'],
        ))
        ->condition('lid', $location['lid'])
        ->execute();
    break;

    case 'defaults':
      return array(
        'region' => array('default' => '', 'collect' => 0, 'weight' => 9, 'widget' => 'dropdown'),
      );
  }
}

/**
 * AJAX callback for the Country select form.
 */
function location_region_country_ajax_callback($form, $form_state) {
  // Country select is the triggering element. Find the province field in the
  // same location fieldset.
  $country_field = $form_state['triggering_element'];
  $location_path = $form_state['triggering_element']['#array_parents'];

  array_pop($location_path);

  // Do not process it for settings field.
  if ($location_path[0] == 'field') {
    return;
  }

  $location_field = drupal_array_get_nested_value($form, $location_path);

  $province = $location_field['province'];
  $region = $location_field['region'];
  $country = $location_field['country'];
  $country_code = strtolower($country_field['#value']);

  $wrapper_region = $country['#ajax']['wrapper'];
  $wrapper_province = $region['#ajax']['wrapper'];

  $regions = location_region_get_regions($country_code);
  if (count($regions)) {
    $region['#type'] = 'select';
    $region['#options'] = array('' => t('Select')) + $regions;
    $province['#options'] = array('' => t('Select'));
  }
  else {
    // Set new options from triggering element value.
    $province['#options'] = array('' => t('Select'), 'xx' => t('NOT LISTED')) + location_get_provinces($country_field['#value']);
  }

  if ($country_code) {
    module_load_include('inc', 'location_region', "supported/location_region.regions.{$country_code}");
    if (function_exists("location_region_{$country_code}_region_label")) {
      $region['#title'] = call_user_func("location_region_{$country_code}_region_label");
      $province['#title'] = call_user_func("location_region_{$country_code}_province_label");
    }
  }

  // Replace both Region and Province.
  $commands = array();
  $commands[] = ajax_command_replace("#{$wrapper_region}", render($region));
  $commands[] = ajax_command_replace("#{$wrapper_province}", render($province));

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Implements hook_element_info_alter().
 */
function location_region_element_info_alter(&$type) {
  $type['location_element']['#process'][] = 'location_region_location_element_process';
}

/**
 * Process the location element.
 */
function location_region_location_element_process($element, $form_state) {
  // Get selected Country.
  if (isset($element['value']['country'])) {
    $country = $element['#value']['country'];
  }
  else {
    $country = isset($element['country']['#default_value']) ? $element['country']['#default_value'] : '';
  }

  $regions = location_region_get_regions($country);
  if (count($regions)) {
    $regions = array('' => t('Select')) + $regions;
    $element['region']['#options'] = $regions;

    // Get selected Region.
    if (isset($element['value']['region'])) {
      $region = $element['#value']['region'];
    }
    else {
      $region = isset($element['region']['#default_value']) ? $element['region']['#default_value'] : '';
    }

    $provinces = location_region_get_provinces($region, $country);
    $element['province']['#options'] = array('' => t('Select')) + $provinces;
  }
  else {
    $element['region']['#value'] = '';
    $element['region']['#type'] = 'hidden';

    $provinces = location_get_provinces($country);
    $element['province']['#options'] = array('' => t('Select'), 'xx' => t('NOT LISTED')) + $provinces;
  }

  if ($country) {
    module_load_include('inc', 'location_region', "supported/location_region.regions.{$country}");
    if (function_exists("location_region_{$country}_province_label")) {
      $element['province']['#title'] = call_user_func("location_region_{$country}_province_label");
      $element['region']['#title'] = call_user_func("location_region_{$country}_region_label");
    }
  }

  return $element;
}

/**
 * AJAX callback for the Region select form.
 */
function location_region_region_callback($form, $form_state) {
  $location_path = $form_state['triggering_element']['#array_parents'];
  array_pop($location_path);
  $location_field = drupal_array_get_nested_value($form, $location_path);
  $province = $location_field['province'];

  $country_code = $location_field['country']['#value'];

  if ($country_code) {
    module_load_include('inc', 'location_region', "supported/location_region.regions.{$country_code}");
    if (function_exists("location_region_{$country_code}_province_label")) {
      $province['#title'] = call_user_func("location_region_{$country_code}_province_label");
    }
  }

  $region = $location_field['region'];
  $province['#options'] = array('' => t('Select')) + location_region_get_provinces($region['#value'], $country_code);

  return $province;
}

/**
 * Get Regions by country.
 */
function location_region_get_regions($country = '') {
  $country = strtolower($country);
  module_load_include('inc', 'location_region', "supported/location_region.regions.{$country}");

  if (function_exists("location_region_get_regions_{$country}")) {
    return call_user_func("location_region_get_regions_{$country}");
  }

  return array();
}

/**
 * Get FR Departments by Region
 */
function location_region_get_provinces($region, $country = '') {
  $country = strtolower($country);
  module_load_include('inc', 'location_region', "supported/location_region.regions.{$country}");

  if (function_exists("location_region_get_provinces_{$country}")) {
    return call_user_func("location_region_get_provinces_{$country}", $region);
  }

  return array();
}

/**
 * Implements hook_install().
 */
function location_region_install() {
  $ret = array();
  $schema['location'] = array();
  location_region_schema_alter($schema);
  foreach ($schema['location']['fields'] as $name => $spec) {
    db_add_field('location', $name, $spec);
  }
}

/**
 * Implements hook_uninstall().
 */
function location_region_uninstall() {
  db_drop_field('location', 'region');
}

/**
 * Implements hook_schema_alter().
 */
function location_region_schema_alter(&$schema) {
  $schema['location']['fields']['region'] = array(
    'description' => 'Region',
    'type' => 'varchar',
    'length' => 255,
    'default' => '',
    'not null' => TRUE,
  );
}

/**
 * Implements hook_views_data_alter().
 */
function location_region_views_data_alter(&$data) {
  $data['location']['region'] = array(
    'title' => t('Region'),
    'help' => t('The region of the selected location.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'location_region_handler_filter_location_region',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Alter existing province filter.
  $data['location']['province']['filter'] = array(
    'handler' => 'location_region_handler_filter_location_province',
  );
}

/**
 * Implements hook_theme_registry_alter().
 */
function location_region_theme_registry_alter(&$theme_registry) {
  $theme_registry['location']['template'] = drupal_get_path('module', 'location_region') . '/location';
}

/**
 * Implements hook_location_provinces_alter().
 */
function location_region_location_provinces_alter(&$provinces, $country) {
  if ($country == 'lb') {
    $provinces[$country] = array();

    $country = strtolower($country);
    module_load_include('inc', 'location_region', "supported/location_region.regions.{$country}");

    $districts = location_region_get_provinces_lb();
    foreach ($districts as $governorate) {
      foreach ($governorate as $code => $province) {
        $provinces[$country][$code] = $province;
      }
    }
  }
}