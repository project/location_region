<?php

/**
 * Get Lebanon Governorates.
 */
function location_region_get_regions_lb() {
  $regions = array(
    'BA' => 'Beirut',
    'BQ' => 'Beqaa',
    'JL' => 'Mount Lebanon',
    'NA' => 'Nabatieh',
    'NL' => 'North',
    'JA' => 'South',
  );

  return $regions;
}

/**
 * Get Lebanon Provinces by Region.
 */
function location_region_get_provinces_lb($region = FALSE) {
  // Define de region/provinces array.
  $region_departments = array(
    'BA' => array(
      'BA' => 'Beirut',
    ),
    'BQ' => array(
      'BB' => 'Baalbek',
      'HE' => 'Hermel',
      'RA' => 'Rachiaya',
      'BW' => 'West Bekaa',
      'ZA' => 'Zahle',
    ),
    'JL' => array(
      'AL' => 'Aley',
      'BD' => 'Baabda',
      'CH' => 'Chouf',
      'MT' => 'El Metn',
      'JB' => 'Jubail',
      'KE' => 'Kasrouane',
    ),
    'NA' => array(
      'BJ' => 'Bint Jubail',
      'HA' => 'Hasbaiya',
      'MJ' => 'Marjaayoun',
      'NA' => 'Nabatiye',
    ),
    'NL' => array(
      'AK' => 'Akkar',
      'BT' => 'Batroun',
      'BC' => 'Bcharre',
      'KO' => 'Koura',
      'MD' => 'Minieh-Danieh',
      'TR' => 'Tripoli',
      'ZG' => 'Zgharta',
    ),
    'JA' => array(
      'JE' => 'Jezzine',
      'SA' => 'Saida',
      'SU' => 'Sour',
    ),
  );

  if (! $region) {
    return $region_departments;
  }

  if (isset($region_departments[$region])) {
    return $region_departments[$region];
  }

  return array();
}

/**
 * Get the label for Region of France.
 */
function location_region_lb_region_label() {
  return t('Governorate');
}

/**
 * Get the labels for Province of France.
 */
function location_region_lb_province_label() {
  return t('District');
}